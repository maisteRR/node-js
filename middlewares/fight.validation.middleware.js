const { fight } = require('../models/fight');
const { isEmpty: isFieldEmpty } = require('validator');

const isBodyEqualModel = (body, fight) => {
    return Object.keys(body).every((prop) => {
        return fight.hasOwnProperty(prop) && prop !== 'id';
    });
}
const isFightDataValid = (body) =>{
    if(!isBodyEqualModel(body, fight)) return false;

    const {fighter1, fighter2, log} = body;

    if (
        !!fighter1 &&
        !isFieldEmpty(fighter1) &&

        !!fighter2 &&
        !isFieldEmpty(fighter2) &&

        !!log
    ) return true;
    else {
        return false;
    }
}
const createFightValid = (req, res, next) => {

    if(isFightDataValid(req.body)){
        next();
    }
    else {
        res.err = Error('Unable to create fight. Invalid data.');
        res.err.status = 400;
        next();
    }
}
const updateFightValid = (req, res, next) => {
    if(isFightDataValid(req.body)){
        next();
    }
    else {
        res.err = Error('Unable to update fight. Invalid data.');
        res.err.status = 400;
        next();
    }
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;