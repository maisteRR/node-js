const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    searchAll(){
        const items = FightRepository.getAll();
        if(!items.length) {
            return false;
        }
        else return items;
    }
    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return false;
        }
        return item;
    }
    add(fight) {
        const item = FightRepository.create(fight);
        if(!item) {
            let err = Error('Unable to create a new fight.');
            err.status = 400;
            throw err;
        }
        return item;
    }
    update(id, fight){
        if(!this.search({id})){
            let err = Error('Fight not found. Unable to update.');
            err.status = 404;
            throw err;
        }
        else{
            const item = FightRepository.update(id, fight);
            if(!item){
                let err = Error('Unable to update fight.');
                err.status = 400;
                throw err;
            }
            else return item;
        }
    }
    delete(id){
        if(!this.search({id})){
            let err = Error('Fight not found. Unable to delete.');
            err.status = 404;
            throw err;
        } else{
            const item = FightRepository.delete(id);
            if(!item.length){
                let err = Error('Unable to delete fight.');
                err.status = 400;
                throw err;
            }
            else return item;
        }
    }
}

module.exports = new FightersService();