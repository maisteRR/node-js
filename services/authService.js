const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            let err = Error('User not found.');
            err.status = 404;
            throw err;
        }
        return user;
    }
}

module.exports = new AuthService();