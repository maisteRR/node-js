import {get, post} from "../requestHelper";

const entity = 'fight';

export const getFightHistory = async () => {
    return await get(entity);
}
export const createFightHistory = async (body) => {
    return await post(entity, body);
}