import { TextField } from "material-ui"
import { createFighter } from "../../services/domainRequest/fightersRequest";
import React, { useState } from "react";
import { Button } from "@material-ui/core";
import './newFighter.css';

export default function NewFighter({ onCreated }) {
    const [name, setName] = useState('');
    const [health, setHealth] = useState('');
    const [power, setPower] = useState('');
    const [defense, setDefense] = useState('');
    const [source, setImageSource] = useState('');

    const onNameChange = (event) => {
        setName(event.target.value);
    }
    const onHealthChange = (event) => {
        setHealth(event.target.value);
    }
    const onPowerChange = (event) => {
        setPower(event.target.value);
    }
    const onDefenseChange = (event) => {
        setDefense(event.target.value);
    }
    const onImageSourceChange = (event) => {
        setImageSource(event.target.value);
    }
    const resetForm = () => {
        setDefense('');
        setPower('');
        setHealth('');
        setName('');
        setImageSource('');
    }
    const onSubmit = async () => {
        const data = await createFighter({ name, health, power, defense, source });
        if(data && !data.error) {
            onCreated(data);
            resetForm();
        }
    }

    return (
        <div id="new-fighter">
            <div>New Fighter</div>
            <TextField onChange={onNameChange} value={name} id="standard-basic" label="Standard" placeholder="Name"/>
            <TextField onChange={onHealthChange} value={health} id="standard-basic" label="Standard" placeholder="Health" type="number" />
            <TextField onChange={onPowerChange} value={power} id="standard-basic" label="Standard" placeholder="Power" type="number" />
            <TextField onChange={onDefenseChange} value={defense} id="standard-basic" label="Standard" placeholder="Defense" type="number" />
            <TextField onChange={onImageSourceChange} value={source} id="standard-basic" label="Standard" placeholder="Image url"/>
            <Button onClick={onSubmit} variant="contained" color="primary">Create</Button>
        </div>
    );
};