const {FighterRepository} = require('../repositories/fighterRepository');

class FighterService {
    searchAll(){
        const items = FighterRepository.getAll();
        if(!items.length) {
            return false;
        }
        else return items;
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return false;
        }
        else return item;
    }

    add(fighter) {
        if (this.search({name: fighter.name})) {
            let err = Error('Fighter already exists.');
            err.status = 400;
            throw err;
        } else {
            const item = FighterRepository.create(fighter);
            if (!item) {
                let err = Error('Unable to create a new fighter.');
                err.status = 400;
                throw err;
            } else return item;
        }
    }
    update(id, fighter){
        if(!this.search({id})){
            let err = Error('Fighter not found. Unable to update.');
            err.status = 404;
            throw err;
        }
        else{
            const item = FighterRepository.update(id, fighter);
            if(!item){
                let err = Error('Unable to update fighter.');
                err.status = 400;
                throw err;
            }
            else return item;
        }

    }
    delete(id){
        if(!this.search({id})){
            let err = Error('Fighter not found. Unable to delete.');
            err.status = 404;
            throw err;
        } else{
            const item = FighterRepository.delete(id);
            if(!item.length){
                let err = Error('Unable to delete fighter.');
                err.status = 400;
                throw err;
            }
            else return item;
        }
    }
}

module.exports = new FighterService();