const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');
const router = Router();

router.get('/', (req, res, next) => {
    try {
        const users = UserService.searchAll();
        if(users) res.data = users;
        else {
            let err = Error('No users found in database.');
            err.status = 404;
            throw err;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const user = UserService.search({id: req.params.id});
        if(user) res.data = user;
        else{
            let err = new Error('User not found.');
            err.status = 404;
            throw err;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
    try {
        if (res.err) next();
        else res.data = UserService.add(req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if (res.err) next();
        else res.data = UserService.update(req.params.id, req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = UserService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;