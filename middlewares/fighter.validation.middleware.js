const {fighter} = require('../models/fighter');
const {isEmpty: isFieldEmpty, isInt} = require('validator');

const isBodyEqualModel = (body, fighter) => {
    return Object.keys(body).every((prop) => {
        return fighter.hasOwnProperty(prop) && prop !== 'id';
    });
}
const isImageValid = (source) => {
    const extension = source.split('.').pop();
    return ['png', 'gif', 'jpg'].some((imageExt) => {
        return imageExt === extension;
    })
}
const normalizeFighterCharacteristics = (body) => {
    body.power = Number(body.power);
    body.defense = Number(body.defense);
    body.health = 100;
    body.health = body.health || 100;
}
const isFighterDataValid = (body) => {
    if (!isBodyEqualModel(body, fighter)) return false;
    const {name, health, power, defense, source} = body;

    if (
        !!name &&
        !isFieldEmpty(name) &&

        !!health &&
        !isFieldEmpty(health) &&
        isInt(health) &&
        health > 0 &&
        health < 100 &&

        !!power &&
        !isFieldEmpty(power) &&
        isInt(power) &&
        power > 0 &&
        power < 100 &&

        !!defense &&
        !isFieldEmpty(defense) &&
        isInt(defense) &&
        defense >= 1 &&
        defense <= 10 &&

        !!source &&
        !isFieldEmpty(source) &&
        isImageValid(source)

    ) return true;
    else {
        return false;
    }
}
const createFighterValid = (req, res, next) => {
    if (isFighterDataValid(req.body)) {
        normalizeFighterCharacteristics(req.body);
        req.body.source = req.body.source;
        next();
    } else {
        res.err = Error('Unable to create fighter. Invalid data.');
        res.err.status = 400;
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    if (isFighterDataValid(req.body)) {
        normalizeFighterCharacteristics(req.body);
        next();
    } else {
        res.err = Error('Unable to update fighter. Invalid data.');
        res.err.status = 400;
        next();
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;