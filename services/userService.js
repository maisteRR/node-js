const { UserRepository } = require('../repositories/userRepository');

class UserService {

    searchAll(){
        const items = UserRepository.getAll();
        if(!items.length) {
            return false;
        }
        else return items;
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return false;
        }
        return item;
    }
    add(user) {
        if(this.search({email: user.email})){
            let err = Error('User already exists.');
            err.status = 400;
            throw err;
        }
        else{
            const item = UserRepository.create(user);
            if(!item) {
                let err = Error('Unable to create a new user.');
                err.status = 400;
                throw err;
            }
            else return item;
        }
    }
    update(id, user){
        if(!this.search({id})){
            let err = Error('User not found. Unable to update.');
            err.status = 404;
            throw err;
        }
        else{
            const item = UserRepository.update(id, user);
            if(!item){
                let err = Error('Unable to update user.');
                err.status = 400;
                throw err;
            }
            else return item;
        }

    }
    delete(id){
        if(!this.search({id})){
            let err = Error('User not found. Unable to delete.');
            err.status = 404;
            throw err;
        } else{
            const item = UserRepository.delete(id);
            if(!item.length){
                let err = Error('Unable to delete user.');
                err.status = 400;
                throw err;
            }
            else return item;
        }
    }
}

module.exports = new UserService();