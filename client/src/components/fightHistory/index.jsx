import React from 'react';
import {createFightHistory, getFightHistory} from '../../services/domainRequest/fightRequest';
import FightHistoryItem from "./FightHistoryItem";

class FightHistory extends React.Component {
    state = {
        fights: []
    };

    async componentDidMount() {
        const fights = await getFightHistory();
        if(fights && !fights.error) {
            this.setState({ fights });
        }
    }
    static onFightEnd = async (fight) => {
        const {fighter1, fighter2, log} = fight;
        const data = await createFightHistory({ fighter1, fighter2, log });
        if(data && !data.error) {
            console.log('All right');
        }
    }
    render() {
        return <div>
            <h1>Fights history</h1>
            {this.state.fights.length ? this.state.fights.map((item)=>{
                return <FightHistoryItem props={item} />}) : null}
        </div>
    }
}

export default FightHistory;