import {controls} from '../../constants/controls';
import FightHistory from '../../../components/fightHistory/index';

class LogFight {
    constructor() {
        this.fighter1 = null;
        this.fighter2 = null;
        this.log = [];
    }

    setFighter1(fighter) {
        this.fighter1 = fighter;
    }

    setFighter2(fighter) {
        this.fighter2 = fighter;
    }

    getFighter1() {
        return this.fighter1;
    }

    getFighter2() {
        return this.fighter2;
    }

    addLogItem(logItem) {
        this.log.push(logItem);
    }

    getLog() {
        return this.log;
    }

    toOverGame() {
        return FightHistory.onFightEnd({
            fighter1: this.getFighter1().name,
            fighter2: this.getFighter2().name,
            log: this.getLog()
        })
    }
}

const currentFight = new LogFight();

export async function fight(firstFighter, secondFighter) {
    currentFight.setFighter1(firstFighter);
    currentFight.setFighter2(secondFighter);

    updateFightersHealth(firstFighter, secondFighter); // ініціалізація health-індикаторів

    return new Promise((resolve) => {
        firstFighter.state = {
            blocking: false,
            criticalHitStatus: 0,
            isCriticalHitAllowed: true,
            criticalHitCombination: controls.PlayerOneCriticalHitCombination
        };
        secondFighter.state = {
            blocking: false,
            criticalHitStatus: 0,
            isCriticalHitAllowed: true,
            criticalHitCombination: controls.PlayerTwoCriticalHitCombination

        };
        document.addEventListener('keyup', (e) => {
            getCriticalHitStatus(firstFighter, e, 'keyup');
            getCriticalHitStatus(secondFighter, e, 'keyup');

            switch (isSomeoneWon(firstFighter, secondFighter)) {
                case firstFighter:
                    currentFight.toOverGame().then(() => {
                        resolve(firstFighter);
                    })
                    break;
                case secondFighter:
                    currentFight.toOverGame().then(() => {
                        resolve(secondFighter);
                    })
                    break;
                default:
                    break;
            }
            switch (e.code) {
                case controls.PlayerOneAttack:
                    if (firstFighter.state.blocking) break;
                    else {
                        fighterKick(firstFighter, secondFighter);
                        updateFightersHealth(firstFighter, secondFighter);
                    }
                    break;
                case controls.PlayerTwoAttack:
                    if (secondFighter.state.blocking) break;
                    else {
                        fighterKick(secondFighter, firstFighter);
                        updateFightersHealth(firstFighter, secondFighter);
                    }
                    break;
                case controls.PlayerTwoBlock:
                    secondFighter.state.blocking = false;
                    break;
                case controls.PlayerOneBlock:
                    firstFighter.state.blocking = false;
                    break;

                default:
                    break;
            }
        });
        document.addEventListener('keydown', (e) => {
            updateFightersHealth(firstFighter, secondFighter);

            if (getCriticalHitStatus(firstFighter, e, 'keydown')) {
                animateCriticalHit();
                secondFighter.health -= getCriticalHitPower(firstFighter);
            }

            if (getCriticalHitStatus(secondFighter, e, 'keydown')) {
                animateCriticalHit();
                firstFighter.health -= getCriticalHitPower(secondFighter);
            }
            switch (e.code) {
                case controls.PlayerOneBlock:
                    firstFighter.state.blocking = true;
                    break;
                case controls.PlayerTwoBlock:
                    secondFighter.state.blocking = true;
                    break;
                default:
                    break;
            }
        });
    });
}

function fighterKick(attacker, defender) {

    animateFight('running');
    if (defender.state.blocking)
        defender.health -= getDamage(attacker, defender);
    else {
        defender.health -= getHitPower(attacker);
    }
    // TODO: add valid data to object
    currentFight.addLogItem(
      {
        fighter1Shot: 20,
        fighter2Shot: 10,
        fighter1Health: 90,
        fighter2Health: 30
      })
}

function getDamage(attacker, defender) {
    const summaryPower = getHitPower(attacker);
    const summaryDefence = getBlockPower(defender);
    if (summaryPower > summaryDefence)
        return summaryPower - summaryDefence;
    else return 0;
}

function getCriticalHitPower(attacker) {
    return attacker.power * 2;
}

function getHitPower(fighter) {
    const criticalHitChance = Math.random() + 1;
    return fighter.power * criticalHitChance;
}

function getBlockPower(fighter) {
    const dodgeChance = Math.random() + 1;
    return fighter.defense * dodgeChance;
}

function updateFightersHealth(firstFighter, secondFighter) {
    const indicators = document.getElementsByClassName('arena___health-bar');
    if (firstFighter.health < 20)
        indicators[0].style.backgroundColor = 'red';
    if (secondFighter.health < 20)
        indicators[1].style.backgroundColor = 'red';
    indicators[0].style.width = firstFighter.health + '%';
    indicators[1].style.width = secondFighter.health + '%';
}


let isGameOver = false;

function isSomeoneWon(firstFighter, secondFighter) {
    if (firstFighter.health <= 0 && !isGameOver) {
        isGameOver = true;
        return secondFighter;
    } else if (secondFighter.health <= 0 && !isGameOver) {
        isGameOver = true;
    } else return false;
}

function getCriticalHitStatus(fighter, event, eventName) {
    if (eventName === 'keyup' && !fighter.state.blocking) {
        fighter.state.criticalHitCombination.forEach((criticalKey) => {
            if (event.code === criticalKey) fighter.state.criticalHitStatus = 0;
        });
    } else if (eventName === 'keydown') {
        if (!event.repeat && !fighter.state.blocking) {
            fighter.state.criticalHitCombination.forEach((criticalKey) => {
                if (event.code === criticalKey) fighter.state.criticalHitStatus++;
            });
        }
    }
    if (fighter.state.criticalHitStatus === 2 && fighter.state.isCriticalHitAllowed) {
        fighter.state.isCriticalHitAllowed = false;
        let criticalHitTimeout = setTimeout(() => {
            fighter.state.isCriticalHitAllowed = true;
            clearTimeout(criticalHitTimeout);
            criticalHitTimeout = null;
        }, 10000);
        return true;
    } else return false;
}

function animateFight(status) {
    const arena = document.getElementsByClassName('arena___battlefield')[0];
    arena.style.animationPlayState = status;
    if (status === 'running') {
        let animationTimer = setTimeout(() => {
            arena.style.animationPlayState = 'paused';
            clearTimeout(animationTimer);
            animationTimer = null;
        }, 300);
    }
}

function animateCriticalHit() {
    animateFight('running');
    const fighters = document.getElementsByClassName('arena___fighter');
    fighters[0].style.transform = 'rotate(30deg)';
    fighters[1].style.transform = 'rotate(-30deg)';
    let animationTimer = setTimeout(() => {
        fighters[0].style.transform = 'rotate(0deg)';
        fighters[1].style.transform = 'rotate(0deg)';
        clearTimeout(animationTimer);
        animationTimer = null;
    }, 300);
}