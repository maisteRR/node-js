import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(12),
        fontWeight: theme.typography.fontWeightRegular,
    },
});

class FightHistoryItem extends React.Component {
    render() {
        const {classes} = this.props;
        const {fighter1, fighter2, log} = this.props.props;

        return <Container maxWidth="sm">
        <div className={classes.root}>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    aria-controls="panel1a-content"
                    id="panel1a-header">
                    <Typography className={classes.heading}>{fighter1} VS {fighter2}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid container spacing={3}>
                            {log.map((round, index)=>{
                                return <Grid item xs={6}>
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        aria-controls="panel1a-content"
                                        id="panel1a-header"
                                    >
                                        <Typography className={classes.heading}>Round {index + 1}</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails>
                                        First &#128153;: {round.fighter1Health} <br />
                                        Second &#128153;: {round.fighter2Health} <br />

                                        First &#127993;: {round.fighter1Shot} <br />
                                        Second &#127993;: {round.fighter2Shot} <br />
                                    </ExpansionPanelDetails>

                                </ExpansionPanel>

                                </Grid>
                            })}
                    </Grid>

            </ExpansionPanelDetails>
        </ExpansionPanel>
    </div>
            </Container>
    }
}

export default withStyles(styles, { withTheme: true })(FightHistoryItem);