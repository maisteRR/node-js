import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal(
    {
      title: `Congratulations, ${fighter.name}`,
      bodyElement: `With sweat and blood, the fighter ${fighter.name} dumped his opponent.`,
      onClose(){
        window.location.reload(); // метод reload() об'єкту location перезавантажить сторінку і почне гру спочатку
      }
    });
}