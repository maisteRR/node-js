const responseMiddleware = (req, res, next) => {
    if(res.err){
        res.err.status = res.err.status || 500;
        res.data = {
            error: true,
            message: res.err.message || 'Internal server error.'
        }
        res.status(res.err.status).json(res.data);
    }
    else{
        res.json(res.data);
    }
}

exports.responseMiddleware = responseMiddleware;