const { user } = require('../models/user');
const {isEmpty: isFieldEmpty, isEmail, isMobilePhone} = require('validator');

const isBodyEqualModel = (body, user) => {
    return Object.keys(body).every((prop) => {
        return user.hasOwnProperty(prop) && prop !== 'id';
    });
}
const isUserDataValid = (body) =>{
    if(!isBodyEqualModel(body, user)) return false;

    const {firstName, lastName, email, phoneNumber, password} = body;

    if (
        !!firstName &&
        !isFieldEmpty(firstName) &&

        !!lastName &&
        !isFieldEmpty(lastName) &&

        !!email &&
        !isFieldEmpty(email) &&
        isEmail(email) &&
        email.endsWith('@gmail.com') &&

        !!phoneNumber &&
        !isFieldEmpty(phoneNumber) &&
        isMobilePhone(phoneNumber, 'uk-UA', {strictMode: true}) &&

        !!password &&
        !isFieldEmpty(password) &&
        password.length >= 3
    ) return true;
    else {
        return false;
    }
}
const createUserValid = (req, res, next) => {

    if(isUserDataValid(req.body)){
        next();
    }
    else {
        res.err = Error('Unable to create user. Invalid data.');
        res.err.status = 400;
        next();
    }
}
const updateUserValid = (req, res, next) => {
    if(isUserDataValid(req.body)){
        next();
    }
    else {
        res.err = Error('Unable to update user. Invalid data.');
        res.err.status = 400;
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;