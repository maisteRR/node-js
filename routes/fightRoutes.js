const {createFightValid, updateFightValid} = require("../middlewares/fight.validation.middleware");
const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        const fights = FightService.searchAll();
        if(fights) res.data = fights;
        else {
            let err = Error('No fights found in database.');
            err.status = 404;
            throw err;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const fight = FightService.search({id: req.params.id});
        if(fight) res.data = fight;
        else{
            let err = new Error('Fight not found.');
            err.status = 404;
            throw err;
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFightValid, (req, res, next) => {
    try {
        if (res.err) next();
        else res.data = FightService.add(req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', updateFightValid, (req, res, next) => {
    try {
        if (res.err) next();
        else res.data = FightService.update(req.params.id, req.body);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        res.data = FightService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;